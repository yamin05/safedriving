//
//  MotionManager.h
//  SafeDriving
//
//  Created by James Zhan on 30/4/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <CoreMotion/CoreMotion.h>
#import <React/RCTEventEmitter.h>

@interface DeviceMotion : RCTEventEmitter <RCTBridgeModule>{
  CMMotionManager *_motionManager;
}
- (void) setUpdateInterval:(double) interval;
- (void) startDeviceMotionUpdates;
- (void) stopDeviceMotionUpdates;
@end
