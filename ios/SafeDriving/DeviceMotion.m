//
//  MotionManager.m
//  SafeDriving
//
//  Created by James Zhan on 30/4/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "DeviceMotion.h"

@implementation DeviceMotion
RCT_EXPORT_MODULE();

- (id) init {
  self = [super init];
  self->_motionManager = [[CMMotionManager alloc] init];
  return self;
}
- (NSArray<NSString *> *)supportedEvents
{
  return @[@"DeviceMotion"];
}
RCT_EXPORT_METHOD(setUpdateInterval:(double) interval) {
  [self->_motionManager setDeviceMotionUpdateInterval:interval];
}
RCT_EXPORT_METHOD(stopDeviceMotionUpdates) {
  [self->_motionManager stopDeviceMotionUpdates];
}
RCT_EXPORT_METHOD(startDeviceMotionUpdates) {
  NSLog(@"called!!!!");
  [self->_motionManager startDeviceMotionUpdatesUsingReferenceFrame: CMAttitudeReferenceFrameXArbitraryCorrectedZVertical
                                                        toQueue:[NSOperationQueue mainQueue]
                                                        withHandler:^(CMDeviceMotion *motion, NSError *error)
   {
     [self sendEventWithName:@"DeviceMotion" body:@{
                                                                             @"timestamp" : [NSNumber numberWithDouble:motion.timestamp],
                                                                             @"rotationRate": @{
                                                                                 @"x" : [NSNumber numberWithDouble:motion.rotationRate.x],
                                                                                 @"y" : [NSNumber numberWithDouble:motion.rotationRate.y],
                                                                                 @"z" : [NSNumber numberWithDouble:motion.rotationRate.z]
                                                                                 },
                                                                             @"userAcceleration": @{
                                                                                 @"x" : [NSNumber numberWithDouble:motion.userAcceleration.x],
                                                                                 @"y" : [NSNumber numberWithDouble:motion.userAcceleration.y],
                                                                                 @"z" : [NSNumber numberWithDouble:motion.userAcceleration.z]
                                                                                 },
                                                                             @"gravity": @{
                                                                                 @"x" : [NSNumber numberWithDouble:motion.gravity.x],
                                                                                 @"y" : [NSNumber numberWithDouble:motion.gravity.y],
                                                                                 @"z" : [NSNumber numberWithDouble:motion.gravity.z]
                                                                                 },
                                                                             @"magneticField": @{
                                                                                 @"x" : [NSNumber numberWithDouble:motion.magneticField.field.x],
                                                                                 @"y" : [NSNumber numberWithDouble:motion.magneticField.field.y],
                                                                                 @"z" : [NSNumber numberWithDouble:motion.magneticField.field.z],
                                                                                 @"accuracy" : [NSNumber numberWithDouble:motion.magneticField.accuracy]
                                                                                 }
                                                                            }
      ];}];
   }
@end
