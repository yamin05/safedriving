Install dependencies
1. [node](https://nodejs.org/en/)
2. react-native-cli: in terminal, run `npm install -g react-native-cli`
3. npm package: use terminal, change directory to the project root, run `npm install`
4. start react native server: change directory to the project root, run `npm start`
5. open the xcode project, located in ios folder
