import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-flex-modal';
import Counter from './Counter';

export default class Acquire extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{ flex: 1,
          paddingTop: 20,
          alignItems: 'center',
          justifyContent: 'center' }}
      >
        <TouchableOpacity
          style={{ width: 200,
            height: 200,
            borderRadius: 100,
            backgroundColor: '#42A5F5',
            alignItems: 'center',
            justifyContent: 'center' }}
          onPress={() => this.modal.open()}
        >
          <Text style={{ fontSize: 50 }}>Start</Text>
        </TouchableOpacity>
        <Modal
          ref={(modal) => { this.modal = modal; }}
          positionYIn={Dimensions.get('window').height}
        >
          <Counter onFinish={() => this.modal.close()} />
        </Modal>
      </View>
    );
  }
}

Acquire.navigationOptions = {
  tabBarIcon: ({ focused, tintColor }) => (<Icon name="ios-timer" size={30} color={focused ? tintColor : null} />)
};
