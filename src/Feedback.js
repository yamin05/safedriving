import { View, AsyncStorage, Dimensions, Button, FlatList, Text } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-flex-modal';

import Login from './Login';

export default class Feedback extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
    this.getFeedback = this.getFeedback.bind(this);
  }
  componentWillMount() {
    AsyncStorage.getItem('user').then((value) => {
      if (value === null) {
        this.modal.open();
      }
    });
  }
  async getFeedback() {
    const user = JSON.parse(await AsyncStorage.getItem('user'));
    const payload = {};
    payload.userID = user.userId;
    fetch('http://drvprojectv3.azurewebsites.net/api/History', {
      method: 'POST',
      headers: {
        Authorization: user.token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload)
    })
    .then(res => res.json())
    .then(json => this.setState({ data: json }));
  }
  render() {
    return (
      <View
        style={{ flex: 1,
          paddingTop: 20 }}
      >
        <FlatList
          data={this.state.data}
          renderItem={({ item }) =>
            <View
              style={{ flexDirection: 'row',
                justifyContent: 'space-around' }} key={item.Session_Id}
            >
              <Text>{item.Start_time}</Text>
              <Text>{item.Session_Score}</Text>
            </View>}
        />
        <Button
          onPress={this.getFeedback}
          title="Refresh"
        />
        <Modal
          ref={(modal) => { this.modal = modal; }}
          positionYIn={Dimensions.get('window').height}
        >
          <Login onFinish={() => this.modal.close()} />
        </Modal>
      </View>
    );
  }
}

Feedback.navigationOptions = {
  tabBarIcon: ({ focused, tintColor }) => (<Icon name="feedback" size={30} color={focused ? tintColor : null} />)
};
