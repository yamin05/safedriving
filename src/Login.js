import { View, Dimensions, TextInput, Button, ScrollView, Alert, AsyncStorage } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.signUp = this.signUp.bind(this);
    this.login = this.login.bind(this);
    this.state = {
      username: '',
      password: '',
      email: '',
      age: '',
      gender: 'male',
      signUp: false
    };
  }
  signUp() {
    if (!this.state.username || !this.state.password || !this.state.email || !this.state.age) {
      Alert.alert('Please finish all informations');
    } else {
      fetch('http://drvprojectv3.azurewebsites.net/api/Account', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          UserName: this.state.username,
          Password: this.state.password,
          Email: this.state.email,
          Age: this.state.age,
          Gender: this.state.gender
        })
      })
      .then((res) => {
        if (res.status !== 200) {
          return Promise.reject(Alert.alert('User or Email already exist.'));
        }
        return res.json();
      })
      .then(async (json) => {
        await AsyncStorage.setItem('user', JSON.stringify({
          token: json.token,
          username: json.dbUser.UserName,
          userId: json.dbUser.User_Id,
        }));
        this.props.onFinish();
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }
  login() {
    if (!this.state.username || !this.state.password) {
      Alert.alert('Please finish all informations');
    } else {
      fetch('http://drvprojectv3.azurewebsites.net/api/Login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          UserName: this.state.username,
          Password: this.state.password
        })
      })
      .then((res) => {
        if (res.status !== 200) {
          return Promise.reject(Alert.alert('Please Provide correct User Credentials.'));
        }
        return res.json();
      })
      .then(async (json) => {
        console.log(json);
        await AsyncStorage.setItem('user', JSON.stringify({
          token: json.Token,
          username: json.UserName,
          userId: json.User_Id,
        }));
        this.props.onFinish();
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }
  render() {
    return (
      <ScrollView
        style={{ height: Dimensions.get('window').height,
          width: Dimensions.get('window').width, }}
        bounces={false}
      >
        <View
          style={{ height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
            paddingTop: 20,
            justifyContent: 'center',
            paddingLeft: 20,
            paddingRight: 20,
          }}
        >
          {
          !this.state.signUp &&
          <View>
            <TextInput
              style={{ height: 40,
                borderColor: 'gray',
                borderWidth: 1 }}
              placeholder={'Username'}
              autoCorrect={false}
              autoCapitalize={'none'}
              value={this.state.username}
              onChangeText={username => this.setState({ username })}
            />
            <View style={{ height: 40 }} />
            <TextInput
              style={{ height: 40,
                borderColor: 'gray',
                borderWidth: 1 }}
              placeholder={'Password'}
              secureTextEntry
              value={this.state.password}
              onChangeText={password => this.setState({ password })}
            />
            <View style={{ height: 40 }} />
            <Button
              onPress={this.login}
              title="Login"
            />
            <View style={{ height: 40 }} />
            <Button
              onPress={() => {
                this.setState({
                  signUp: true,
                  username: '',
                  password: '',
                  email: '',
                  age: '',
                  gender: 'male',
                });
              }}
              title="To sign up"
            />
          </View>
        }
          {
          this.state.signUp &&
          <View>
            <TextInput
              style={{ height: 40,
                borderColor: 'gray',
                borderWidth: 1 }}
              placeholder={'Username'}
              autoCorrect={false}
              autoCapitalize={'none'}
              value={this.state.username}
              onChangeText={username => this.setState({ username })}
            />
            <View style={{ height: 40 }} />
            <TextInput
              style={{ height: 40,
                borderColor: 'gray',
                borderWidth: 1 }}
              placeholder={'Password'}
              secureTextEntry
              value={this.state.password}
              onChangeText={password => this.setState({ password })}
            />
            <View style={{ height: 40 }} />
            <TextInput
              style={{ height: 40,
                borderColor: 'gray',
                borderWidth: 1 }}
              placeholder={'Email'}
              autoCorrect={false}
              autoCapitalize={'none'}
              value={this.state.email}
              onChangeText={email => this.setState({ email })}
            />
            <View style={{ height: 40 }} />
            <TextInput
              style={{ height: 40,
                borderColor: 'gray',
                borderWidth: 1 }}
              placeholder={'Age'}
              autoCorrect={false}
              autoCapitalize={'none'}
              value={this.state.age}
              keyboardType={'numeric'}
              onChangeText={age => this.setState({ age })}
            />
            <View style={{ height: 40 }} />
            <View style={{ flexDirection: 'row' }}>
              <View
                style={{ flex: 1,
                  borderColor: this.state.gender === 'male' ? 'blue' : 'white',
                  borderWidth: 1
                }}
              >
                <Button
                  onPress={() => { this.setState({ gender: 'male' }); }}
                  title="Male"
                />
              </View>
              <View
                style={{ flex: 1,
                  borderColor: this.state.gender === 'female' ? 'blue' : 'white',
                  borderWidth: 1 }}
              >
                <Button
                  onPress={() => { this.setState({ gender: 'female' }); }}
                  title="Female"
                />
              </View>
            </View>
            <View style={{ height: 40 }} />
            <Button
              onPress={this.signUp}
              title="Sign up"
            />
            <View style={{ height: 40 }} />
            <Button
              onPress={() => {
                this.setState({
                  signUp: false,
                  username: '',
                  password: '',
                  email: '',
                  age: '',
                  gender: 'male',
                });
              }}
              title="Back to login"
            />
          </View>
        }
        </View>
      </ScrollView>
    );
  }
}
