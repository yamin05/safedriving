import { View, AsyncStorage, Dimensions, Text, TouchableOpacity, Button } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-flex-modal';

import Login from './Login';

export default class Info extends React.Component {
  constructor(props) {
    super(props);
    this.state = { username: '' };
  }
  componentWillMount() {
    this.getName();
  }
  getName() {
    AsyncStorage.getItem('user').then((value) => {
      if (value == null) return;
      value = JSON.parse(value);
      console.log(value);
      this.setState({ username: value.username });
    });
  }
  render() {
    return (
      <View
        style={{ flex: 1,
          paddingTop: 20 }}
      >
        <Text
          style={{ fontSize: 20,
            margin: 15 }}
        >
          Username: {this.state.username}
        </Text>
        <Button
          onPress={() => this.getName()}
          title="Refresh"
        />
        <Button
          onPress={async () => {
            await AsyncStorage.removeItem('user');
            this.modal.open();
          }}
          title="Log out"
        />
        <Modal
          ref={(modal) => { this.modal = modal; }}
          positionYIn={Dimensions.get('window').height}
        >
          <Login onFinish={() => { this.getName(); this.modal.close(); }} />
        </Modal>
      </View>
    );
  }
}

Info.navigationOptions = {
  tabBarIcon: ({ focused, tintColor }) => (<Icon name="person" size={30} color={focused ? tintColor : null} />)
};
