import { TabNavigator } from 'react-navigation';
import { View } from 'react-native';
// import React from 'react';
import Feedback from './Feedback';
import Setting from './Setting';
import Acquire from './Acquire';
import Info from './Info';

const App = TabNavigator({
  Feedback: { screen: Feedback },
  Acquire: { screen: Acquire },
  Setting: { screen: Setting },
  Info: { screen: Info },
});

export default App;
