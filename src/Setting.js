import { View, Text, AsyncStorage, Image } from 'react-native';
import React from 'react';
import { CheckBox } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

export default class Setting extends React.Component {
  constructor(props) {
    super(props);
    this.state = { dataRate: 1 };
  }
  async componentWillMount() {
    try {
      const value = await AsyncStorage.getItem('dataRate');
      if (value !== null) {
        this.setState({ dataRate: parseInt(value, 10) });
      } else {
        await AsyncStorage.setItem('dataRate', '1');
      }
    } catch (error) {
      console.err(error);
    }
  }
  render() {
    const rates = [1, 2, 5, 10];
    return (
      <View
        style={{ flex: 1,
          paddingTop: 20 }}
      >
        <Text
          style={{ fontSize: 20,
            margin: 15 }}
        >
          Data Rate
        </Text>
        {
          rates.map(rate => (<CheckBox
            key={rate}
            title={`${rate}Hz`}
            checked={this.state.dataRate === rate}
            onPress={async () => {
              this.setState({ dataRate: rate });
              await AsyncStorage.setItem('dataRate', `${rate}`);
            }}
          />))
        }
      </View>
    );
  }
}
Setting.navigationOptions = {
  tabBarIcon: ({ focused, tintColor }) => (<Icon name="ios-settings" size={30} color={focused ? tintColor : null} />)
};
