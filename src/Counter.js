import { AsyncStorage, View, Text, TouchableOpacity, Dimensions, NativeEventEmitter, NativeModules, DeviceEventEmitter } from 'react-native';
import React from 'react';
// import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import KeepAwake from 'react-native-keep-awake';

const { DeviceMotion, RNLocation } = NativeModules;
const deviceMotionEmitter = new NativeEventEmitter(DeviceMotion);

export default class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.updateMotion = this.updateMotion.bind(this);
    this.updateLocation = this.updateLocation.bind(this);
    this.upload = this.upload.bind(this);
    this.finish = this.finish.bind(this);
    this.sent = 0;
    this.state = {
      user: {},
      motion: false,
      location: false,
      interval: 0,
      startTime: moment(),
      time: '00:00:00',
      lastPosition: {},
      sessionId: '',
      frequency: 0,
      streetName: '',
      speedLimit: 14
    };
    RNLocation.requestWhenInUseAuthorization();
  }
  async componentWillMount() {
    const user = JSON.parse(await AsyncStorage.getItem('user'));
    const dataRate = await AsyncStorage.getItem('dataRate');
    this.setState({
      user: user,
      sessionId: `${user.userId}-${moment().format()}`,
      frequency: dataRate
    });
    const interval = 1000 / parseInt(dataRate, 10);
    DeviceMotion.setUpdateInterval(1 / dataRate);
    DeviceMotion.startDeviceMotionUpdates();
    RNLocation.startUpdatingLocation();
    RNLocation.setDistanceFilter(1.0);
    this.motionListener = deviceMotionEmitter.addListener(
      'DeviceMotion',
      this.updateMotion);
    this.locationListener = DeviceEventEmitter.addListener(
      'locationUpdated',
      this.updateLocation);
    this.timer = setInterval(this.upload, interval);
  }
  finish() {
    DeviceMotion.stopDeviceMotionUpdates();
    RNLocation.stopUpdatingLocation();
    this.motionListener.remove();
    this.locationListener.remove();
    clearInterval(this.timer);

    const data = {};
    data.userID = this.state.user.userId;
    data.Session_Id = this.state.sessionId;
    fetch('http://drvprojectv3.azurewebsites.net/api/Process', {
      method: 'POST',
      headers: {
        Authorization: this.state.user.token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    });

    this.props.onFinish();
  }
  updateMotion(motion) {
    this.setState({ motion });
  }
  updateLocation(location) {
    this.setState({ location });
    if (this.sent === 0) {
      fetch(`http://overpass-api.de/api/interpreter?data=[out:json];way(around:50,${location.coords.latitude},${location.coords.longitude})["maxspeed"~"."];out;`)
      .then(res => res.json())
      .then((json) => {
        if (json.elements.length > 0) {
          this.setState({
            streetName: json.elements[0].tags.name || '',
            speedLimit: (parseInt(json.elements[0].tags.maxspeed, 10) / 3.6) || 14,
          });
        }
      });
    }
    this.sent = (this.sent + 1) % 20;
  }
  upload() {
    const now = moment();
    const difference = now.diff(this.state.startTime, 'seconds');
    let hour = Math.floor(difference / 3600);
    let min = Math.floor(difference / 60) % 60;
    let sec = difference % 60;
    if (hour < 10)hour = `0${hour}`;
    if (min < 10)min = `0${min}`;
    if (sec < 10)sec = `0${sec}`;
    this.setState({ time: `${hour}:${min}:${sec}` });
    if (this.state.motion && this.state.location) {
      const data = {};
      data.userID = this.state.user.userId;
      data.Session_Id = this.state.sessionId;
      data.frequency = this.state.frequency;
      data.speed = this.state.location.coords.speed;
      data.course = this.state.location.coords.course;
      data.datetime = moment().toISOString();
      data.gravity = this.state.motion.gravity;
      data.userAcceleration = this.state.motion.userAcceleration;
      data.rotationRate = this.state.motion.rotationRate;
      data.magneticField = this.state.motion.magneticField;
      data.coordinate = {
        latitude: this.state.location.coords.latitude,
        longitude: this.state.location.coords.longitude,
        horizontalAccuracy: this.state.location.coords.accuracy
      };
      data.streetName = this.state.streetName;
      data.speedLimit = this.state.speedLimit;
      fetch('http://drvprojectv3.azurewebsites.net/api/Data', {
        method: 'POST',
        headers: {
          Authorization: this.state.user.token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      });
    }
  }
  render() {
    return (
      <View
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          paddingTop: 20,
          alignItems: 'center',
          justifyContent: 'center' }}
      >
        <Text
          style={{ fontSize: 50,
            marginBottom: 20 }}
        >{this.state.time}</Text>
        <TouchableOpacity
          style={{ width: 200,
            height: 200,
            borderRadius: 100,
            backgroundColor: '#F44336',
            alignItems: 'center',
            justifyContent: 'center' }}
          onPress={this.finish}
        >
          <Text style={{ fontSize: 50 }}>Stop</Text>
        </TouchableOpacity>
        <KeepAwake />
      </View>
    );
  }
}
