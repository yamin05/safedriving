/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  NativeEventEmitter,
  NativeModules,
  DeviceEventEmitter
} from 'react-native';
import App from './src/index';

const { DeviceMotion, RNLocation } = NativeModules;
const deviceMotionEmitter = new NativeEventEmitter(DeviceMotion);

export default class SafeDriving extends Component {
  constructor(props) {
    super(props);
    this.state = { data: false,
      location: {} };
    RNLocation.requestWhenInUseAuthorization();
  }
  componentDidMount() {
    DeviceMotion.setUpdateInterval(0.1);
    deviceMotionEmitter.addListener('DeviceMotion', (data) => {
      this.setState({ data });
    });
    DeviceMotion.startDeviceMotionUpdates();
    // RNLocation.startUpdatingLocation();
    // RNLocation.setDistanceFilter(5.0);
    const subscription = DeviceEventEmitter.addListener(
        'locationUpdated',
        (location) => {
          this.setState({ location });
        }
    );
  }
  render() {
    return (
      <View style={styles.container}>
        {
          this.state.data &&
          <View>
            <Text>{this.state.data.timestamp.toFixed(5)}</Text>
            <Text>Gravity</Text>
            <Text>{this.state.data.gravity.x.toFixed(5)}</Text>
            <Text>{this.state.data.gravity.y.toFixed(5)}</Text>
            <Text>{this.state.data.gravity.z.toFixed(5)}</Text>
            <Text>UserAcceleration</Text>
            <Text>{this.state.data.userAcceleration.x.toFixed(5)}</Text>
            <Text>{this.state.data.userAcceleration.y.toFixed(5)}</Text>
            <Text>{this.state.data.userAcceleration.z.toFixed(5)}</Text>
            <Text>RotationRate</Text>
            <Text>{this.state.data.rotationRate.x.toFixed(5)}</Text>
            <Text>{this.state.data.rotationRate.y.toFixed(5)}</Text>
            <Text>{this.state.data.rotationRate.z.toFixed(5)}</Text>
            <Text>MagneticField</Text>
            <Text>{this.state.data.magneticField.x.toFixed(5)}</Text>
            <Text>{this.state.data.magneticField.y.toFixed(5)}</Text>
            <Text>{this.state.data.magneticField.z.toFixed(5)}</Text>
            <Text>{this.state.data.magneticField.accuracy}</Text>
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});

AppRegistry.registerComponent('SafeDriving', () => App);
